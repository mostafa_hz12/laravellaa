<?php

use Illuminate\Support\Facades\Route;
use App\Models\Article;
use phpDocumentor\Reflection\Types\Static_;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/about', function () {


    /*   $articles = Article::all();  */

    $articles = Article::query()->orderByDesc('updated_at')->get();

    return view('about',[
        'articles' => $articles
    ]);


});

Route::get('/contact', function () {
    return view('contact');
});


Route::get('/posts/{post}','PostController@show');



Route::get('/articles','ArticlesController@index')->name('articles.index');
Route::post('/articles','ArticlesController@store');
Route::get('/articles/create','ArticlesController@create');
Route::get('/articles/{article}','ArticlesController@show')->name('articles.show');
Route::get('/articles/{article}/edit','ArticlesController@edit');
Route::put('/articles/{article}','ArticlesController@update');


Route::get('/article/tag/{tag}','ArticleController@tag')->name('articles.tag');

Route::get('/articles/{article}/update','ArticlesController@update');

