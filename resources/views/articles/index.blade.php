
@extends ('layouts.app')

@section('content')

    <div id="wrapper">
        <div id="page" class="container">
            @foreach($articles as $article)
            <div id="content">
                <div class="title">
                    <a href="{{ route('articles.show' , $article->id) }}">
                    <h2> {{  $article->title  }} </h2>
                    </a>
                </div>
                <p><img src="{{asset('assets/images/banner.jpg')}}" alt="" class="image image-full" /> </p>
                {!!  $article->excerpt  !!}
            </div>
            @endforeach
        </div>
    </div>

@endsection
