
@extends ('layouts.app')

@section('content')

    <div id="wrapper">
        <div id="page" class="container">
            <div id="content">
                <div class="title">
                    <h2> {{  $article->title  }} </h2>
                </div>
                <p><img src="{{asset('assets/images/banner.jpg')}}" alt="" class="image image-full" /> </p>
              {{ $article->body }}
                <p style="margin-top:1em">
                    @foreach($article->tags as $tag)

                        <a href="{{ route('articles.tag',[$tag])}}"> {{ $tag->name }}  </a>

                    @endforeach
                </p>
            </div>

        </div>
    </div>

@endsection
