<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Context;

/**
 * Class Post
 * @package App\Models
 * @property int id
 * @property string slug
 * @property Context body
 */

class Post extends Model
{
    use HasFactory;
    protected $fillable = [

        'slug', 'body',


    ];
}
