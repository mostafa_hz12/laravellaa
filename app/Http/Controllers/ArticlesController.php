<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;


class ArticlesController extends Controller
{


    public function index(){
        // render a list of a resource.
        $articles = Article::query()->orderByDesc('updated_at')->get();

        return view('articles.index',[
            'articles' => $articles
        ]);
    }


    public function show(Article $article){
        // show a single resource.

        /* $article = Article::query()->findOrFail($id); */

        return view('articles.show', [
            'article' => $article
        ]);

    }


    public function create(){
        //shows a view to create a new resource.
        return view('articles.create');
    }
    public function store(Request $request){
        //persist the new resource.
        /* dump($_REQUEST); */
        /*  $article = new Article();
          $article -> title = request('title') ;
          $article -> excerpt = request ('excerpt');
          $article -> body = request ('body');
          $article -> save();
          return redirect('/articles'); */

        $request->validate([

            'title' => 'required',
            'excerpt' => 'required',
            'body' => 'required'
        ]);

        /*  $article = new Article();

     $article->title = $request->input('title');
     $article-> excerpt =  $request->input('excerpt');
     $article-> body = $request->input('body');

     $article->save(); */

        Article::query()->create([
            'title' => $request->input('title'),
            'excerpt' => $request->input('excerpt'),
            'body' => $request->input('body')
        ]);
        return redirect(route('articles.index'));



    }
    public function edit($id){
        //show a view to edit an existing resource.
        // find the article associated with id .
        $article = Article::query()->find($id);
        if(!$article){
            abort(404);
        }
        return view('articles.edit', compact('article'));
    }
    public function update(Request $request,$id){
        //persist the edited resource.
        $request->validate( [

            'title' => 'required',
            'excerpt' => 'required',
            'body' => 'required'
        ]);
        $article = Article::query()->find($id);
        $article -> title = $request->input('title') ;
        $article -> excerpt = $request->input ('excerpt');
        $article -> body = $request->input ('body');
        $article -> save();
        return redirect(route('articles.show' , $article->id));
    }
    public function destroy(){
        //Delete the resource.
    }
    public function tag(){

        echo ("baby");
    }
}
