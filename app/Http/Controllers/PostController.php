<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\Types\Static_;









class PostController extends Controller
{
    public function show ($slug)
    {
        $posts = Post::all()->where('slug',$slug)->first();
        if(!$posts){
            abort(404);
        }
        return view('post', [
            'posts' => $posts
        ]);


    }
}
